# Instalación

Clonar el proyecto:

```sh
git clone git@gitlab.com:alanmtk/sube-backend.git
```

Una vez clonado ingresar a la carpeta y desde la raiz del proyecto ejecutar el siguiente comando para instalar las dependencias:

```sh
composer install
```

Generar una clave aleatoria para la aplicación (será usada para encriptar con aes-256-cbc)

```sh
php artisan key:generate
```

Copiar el archivo .env.example a .env, el mismo contiene todas los datos sensibles de configuración y completar la información relacionada a la base de datos:

```sh
DB_HOST=localhost
DB_DATABASE=dbname
DB_USERNAME=username
DB_PASSWORD=password
```

# Uso

Una vez realizada la instalación ya podemos ejecutar el siguiente comando desde la raiz del proyecto:

```sh
php artisan crawl:data
```

El mismo corre las migraciones encargadas de crear la base de datos (en caso de ya existir las tablas las borra y las vuelve a crear). Ademas es el responsable de leer los datos de la API y guardar toda la información en la base de datos.

Para poder hacer uso de la aplicación correr el siguiente comando:

```sh
php artisan serve
```

El mismo levanta el servidor y ejecuta la aplicación. Desde este momento ya tenemos la aplicación corriendo en http://localhost:8000

# Endpoints

Listado de tipos de centros:
```sh
http://localhost:8000/types
```

Listado de provincias:
```sh
http://localhost:8000/provinces 
```

Listado de localidades filtrado por provincia, el código de provincia es obtenible con el endpoint anterior:
```sh
http://localhost:8000/provinces/{code}/cities 
```

Listado de centros filtrado por localidad, el código de localidad es obtenible con el endpoint anterior:
```sh
http://localhost:8000/cities/{id}/venues
```

# To do

+ Agregar los endpoints necesarios
+ Agregar autenticación
