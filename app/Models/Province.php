<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    
	protected $primaryKey = 'code';
	
	public $incrementing = false;

	protected $fillable = ['code', 'name'];

	public function cities()
	{
		return $this->hasMany('App\Models\City', 'province_code');
	}

}
