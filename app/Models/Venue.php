<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{

    protected $fillable = ['address', 'schedules', 'postal_code', 'latitude', 'longitude', 'type_id', 'city_id'];

}
