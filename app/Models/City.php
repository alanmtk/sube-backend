<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['name', 'province_code'];

    public function province()
    {
    	$this->belongsTo('App\Models\Province', 'province_code');
    }

    public function venues()
    {
    	return $this->hasMany('App\Models\Venue');
    }
}
