<?php

use \App\Models\Type;
use \App\Models\City;
use \App\Models\Province;

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::get('types', function(){
	return Type::all();
});

Route::get('provinces', function(){
	return Province::all();
});

Route::get('provinces/{code}/cities', function($code){
	return Province::find($code)->cities()->get();
});

Route::get('cities/{id}/venues', function($id){
	return City::find($id)->venues()->get();
});
